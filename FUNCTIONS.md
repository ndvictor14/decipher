###emacs Decipher Functions

FUNCTION NAME | ACTION
---------|---------------
     pt | inserts an exec block (created by Shaen Toner) that tracks the time respondents spend on each individual page|
     ac | inserts the "allowCountries" tag (to be placed inside survey tag)|
     fc | inserts the "forbiddenCountries" tag (to be placed inside the survey tag)|
     canzip | validates Canadian Postal Codes|
     asoqc | Prevents autosave returning respondents from completing the survey if the quotas have been met. (created by Sandra)|
     tt | turns the selection into a tooltip radio question (SMART interface, no need to create rows ahead of time)|
     wm | inserts the watermark execute block, make sure you have the images in a protected directory !|
     shc | adds 'shuffle="cols"'|
     hideNext | inserts code needed to hide the next part of the question until the previous is answered (insert it inside the question itself people|
     il | inserts image links (<a href ...><img src.....>)|
     gt | insert the "goto" block|
     vzip | Inserts a validate that checks for either 5digit zip codes or extended zip codes|
     qt | inserts standard quota tag|
     bulk | default bulkemail file|
     privacy | code to overide help and privacy policy|
     mob | insert mobile optimization code|
     keywordcode | insert code for keyword coding|
     straighliner | inserts a term based on straighlining|
     clean | removes all id="abcd5" and xlms||
     hideNextMulti | inserts code needed to hide following question until each row has an answer|
     states | Adds a select question with the states as dropdowns (includes DC) (update 7/7/2014| and adds region breakdowns)|
     pi | Smart pipe  makes selection into pipe with subsequent lines as the cases and inserts the default BAD PIPE |
     ca | Makes selected region rows into cases|
     image | Make selected region rows into image tags|
     db | Insert survey code for using a database|
     nextSheet | Insert template code for moving respondents to another quota sheet once the first is full|
     favorite | Insert favorite attribute |
     mg | make groups out of region|
     ag | add groups to rows in region|
     we | add where="execute" attribute|
     v | add virtual tags|
     e | add exec tags|
     speeder| add speeder term based on seconds (prompts for seconds)|