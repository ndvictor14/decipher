;;; Web development clips
;;; Author: Victor Hernandez (vhernan2@alumni.nd.edu)

(require 'cl) ;; for loops

(defun dive ()
  (interactive)
  (insert "<div id=\"\">\n\n</div>"))

(defun div (start end)
  (interactive "r")
  (goto-char end)
  (insert "</div>"))

(defun ule ()
  (interactive)
  (insert "<ul>\n</ul>"))

(defun ul (start end)
  (interactive "r")
  (setq counter (count-lines (start end)))
  (goto-char end)
  (insert "</ul>")
  (goto-char start)
  (insert "<ul>")
  (while (and (< (point) end)
	      (beginning-of-line)
	       (insert "<li>")
	       (end-of-line)
	       (insert "</li>")
	       (forward-line))))
;    (setq counter (counter - 1))))

(defun session
  (interactive)
  (insert "<?php\n// Start the session\nsession_start();\n?>"))

(defun dbconnect
  (interactive)
  (insert ))


