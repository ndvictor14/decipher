(defun oe ()
  ;; Add open end
  (interactive)
  (insert " open=\"1\" openSize=\"25\" randomize=\"0\""))
;;NOTE: ADD ERROR CORRECTION - ENSURE ADDED INSIDE <QTYPE >


(defun alt ()
  " Add alts to selections "
  (interactive)
  (insert "<alt></alt>"))

(defun su ()
  ;; Add suspend
  (interactive)
  (insert "<suspend/>")
)

(defun rr ()
  ;; Add randomize="0" to preven shuffling
  (interactive)
  (insert " randomize=\"0\"")
 ;;NOTE: ADD ERROR CORRECTION - ENSURE ADDED INSIDE <QTYPE >
)

(defun aa ()
  ;; Insert aggregate="0" and percentages="0"
  (interactive)
  (insert " aggregate=\"0\" percentages=\"0\"")
  ;;;;NOTE: ADD ERROR CORRECTION - ENSURE ADDED INSIDE <QTYPE >
)

(defun sh ()
  ;; Insert shuffle rows
  (interactive)
  (insert " shuffle=\"rows\"")
;;NOTE: ADD ERROR CORRECTION - ENSURE ADDED INSIDE <QTYPE >
)

;; This function is new
(defun shc ()
  ;; Insert shuffle cols
  (interactive)
  (insert " shuffle=\"cols\"")
;;NOTE: ADD ERROR CORRECTION - ENSURE ADDED INSIDE <QTYPE >
)

(defun oo ()
  ;; optional="1"
  (interactive)
  (insert " optional=\"1\""))

(defun ee ()
  ;; exclusive="1" randomize="0"
  (interactive)
  (insert " exclusive=\"1\" randomize=\"0\""))

(defun br ()
  ;; add <br>
  (interactive)
  (insert "<br/>"))

(defun mb ()
  ;; add <br><br>
  (interactive)
  (insert "<br/><br/>"))

(defun hideNext ()
  ;; add <script> to hide the next question until the previous is answered
  (interactive)
  (insert "<style name=\"question.after\" wrap=\"ready\" label=\"showQues\" arg:firstQ=\"\" arg:secondQ=\"\">\n<![CDATA[\n  thisQ = $ ('#question_$(firstQ)');\n  nextQ = $ ('#question_$(secondQ)')\n\nif (!thisQ.hasClass(\"hasError\")){\n  nextQ.hide();\n}\n\nthisQ.change(function () {\n  nextQ.show(1000);\n})\n\n]]>\n</style>\n\n"))

(defun il ()
  ;; insert image links
  (interactive)
  (insert "<a href=\"\" target=\"_blank\"><img style=\"border: 0 none;\" src=\"\"/></a>\n"))

(defun gt ()
  ;; adds a goto block
  (interactive)
  (insert "<if cond=\"\">\n  <goto target=\"\"/>\n</if>\n"))

(defun vzip ()
  ;; add a validate for both regular zips and extended zips
  (interactive)
  (insert "<res label=\"zipErr\"></res>\n")
  (insert "<validate>\nif this.val:\n  zip = re.compile(r'\d{5}-\d{4}|\d{5}')\n  if not zip.match(this.val):\n    error(res zipError)\n</validate>"))

(defun res ()
  ;; add a res tag
  (interactive)
  (insert "<res label=\"\"></res>"))

(defun qt ()
  ;; add quota tag
  (interactive)
  (insert "<quota sheet=\"Quotas\"/>\n"))

(defun wm ()
  ;; add watermark execute block
  (interactive)
  (insert "<exec when=\"started\">\np.watermark=\"\"\np.watermarkPosition=\"corner\"\np.watermarkColor=\"white\"\n</exec>\n"))

(defun pt () 
  ;; add exec block for tracking time spent on each page
  ;; Created by Shaen Toner :)
  (interactive)
  (insert "<exec>\np.TIMESTAMP = timeSpent()\n</exec>\n\n<exec when=\"verified\">\nfor x in displayed:\n  try:\n  getattr(TIME_STAMPS, x.label).val = timeSpent() - p.TIMESTAMP\n  except (AttributeError, AssertionError):\n    pass\n\np.TIMESTAMP = timeSpent()\n</exec>\n"))

(defun ac ()
  ;; add the "allowed countries" tag
  (interactive)
  (insert "allowedCountries=\"\"\n")
  (insert "geoip=\"all\"\n"))

(defun fc ()
  ;; add the forbidden countries" tag
  (interactive)
  (insert "forbiddenCountries=\"\"\n")
  (insert "geoip=\"all\"\n"))

(defun canzip ()
  ;; validates for Canadian zipcodes
  (interactive)
  (insert "<res label=\"CanErr\">Please enter a valid postal code in the A0A 0A0 format</res>\n")
  (insert "<validate>\nif re.match(\"^[A-Za-z][0-9][A-Za-z] [0-9][A-Za-z][0-9]$\", this.val) is None:\n  error(res.CanErr)\n</validate>\n"))

(defun asoqc ()
  ;; prevents returning respondents from completing if survey quotas have been met
  ;; code by Sandra
  (interactive)
  (insert "<exec when=\"verified\">\nif hasMarker('/qual'): #update as necessary for an overall qualified marker or some indicator that the respondent has passed the quota sheet\n  cells = gv.survey.root.quota.getQuotaCells\n  markerNames = ['tableElement1','tableElement2']  #fill this in with the primary qualification table\n  for x in range(0,len(markerNames)):\n    if hasMarker('/%s' % (markerNames[x])):\n      current, limit, OQ = cells[ '/%s' % (markerNames[x]) ]\n      if limit le current:\n        setMarker('termbackOQ')\n</exec>\n<style name=\"button.continue\" cond=\"hasMarker('termbackOQ')\" mode=\"instead\"> <![CDATA[\n]]></style>\n<style name=\"button.finish\" cond=\"hasMarker('termbackOQ')\" mode=\"instead\"> <![CDATA[\n]]></style>\n<style name=\"survey.respview.footer.support\" cond=\"hasMarker('termbackOQ')\" mode=\"instead\"> <![CDATA[\n]]><span style=\"font-size: 18px; font-weight:bold; color: red;\">${hlang.get(\"closed\")}</span></style>\n"))

(defun bulk ()
  ;; inserts the bulk email template
  (interactive)
  (insert "From: \"Decipher Inc\" <invite@decipherinc.com>\nSubject: --SUBJECT--\nTo: [email]\nPrecedence: bulk\nMIME-Version: 1.0\nContent-Type: multipart/alternative;\n        boundary=\"----=_NextPart_001_001F_01C26331.F7121560\"\n\n------=_NextPart_001_001F_01C26331.F7121560\nContent-Type: text/plain; charset=\"utf-8\"\n\nDecipher Inc.\n\n${date()}\n\nHello [first] [last],\n\n<!--Message Content Here-->\n\n\nWe respect the personal nature of e-mail communication. If you do not wish to receive mailings from us in the future, you can update your user options at: http://v2.decipherinc.com/survey/optouts?ex=[email]&decLang=english&proj=abc/abc13001.\n\nPlease review our privacy policy at: https://www.focusvision.com/privacy-statement/.\n\nThis e-mail was sent to the following address: [email]\n\nDecipher\n7 Riverpark Place East Suite 110\nFresno, CA 93720\n\n\n------=_NextPart_001_001F_01C26331.F7121560\nContent-Type: text/html;\n        charset=\"utf-8\"\n\n<html>\n<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n</head>\n<body>\n<table border=\"0\" width=\"500\" cellpadding=\"0\" cellspacing=\"0\">\n  <tr> \n    <td>\n      <img src=\"http://static.decipherinc.com/i/logodecipher.gif\" alt=\"Decipher Inc\" width=\"196\" height=\"65\"/>\n      <br/><br/>\n      <div style=\"color: #000000; font-family: verdana; font-size: 8pt;\">\n\n${date()}<br/><br/>\nHello [first] [last],\n<br/><br/>\n\n<!--Message Content Here-->\n\n<br/>\n<br/>\n<br/>\n<hr>\nWe respect the personal nature of e-mail communication. If you do not wish to receive mailings from us in the future, you can update your user options at: \n<a href=\"http://v2.decipherinc.com/survey/optouts?ex=[email]&decLang=english&proj=abc/abc13001\">http://v2.decipherinc.com/survey/optouts?ex=[email]&decLang=english&proj=abc/abc13001</a>.<br/>\n<br/>\nPlease review our privacy policy <a href=\"https://www.focusvision.com/privacy-statement/\">here</a>.\n<br/><br/>\nThis e-mail was sent to the following address: [email]<br/>\n<br/>\nDecipher<br/>\n7 Riverpark Place East Suite 110<br/>\nFresno, CA 93720 \n\n      </div>\n      <img src='http://static.decipherinc.com/tracker/[source]/abc/abc13001/file.gif' align='right'>\n\n    </td>\n  </tr>\n</table>\n</body>\n</html>\n\n------=_NextPart_001_001F_01C26331.F7121560--\n"))

(defun privacy ()
  ;; inserts privacy policy and help text overrides
  (interactive)
  (insert "<res label=\"privacyText\">Privacy Policy</res>\n<res label=\"helpText\">Help</res>\n\n<style name=\"survey.respview.footer.support\">\n  <![CDATA[\n    <a href=\"/survey/privacy?list=1&decLang=${gv.survey.language}\" target=\"_blank\">${res.privacyText}</a> - <a href=\"/support/int/help/${gv.survey.language}\" target=\"_blank\">${res.helpText}</a>\n  ]]>\n</style>"))

(defun mop ()
  ;; inserts code for mobile optimization on grid questions
  (interactive)
  (insert "<style name=\"respview.client.meta\" with=\"Insert Applicable Questions Here\">\n<![CDATA[
\n\@if device.smartphone\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=0.65\"/>\n\@endif\n]]></style>")
  (forward-line)
  (insert "<!--NOTE: In each question tag, you must add 'cs:resize=\"0\" for this to work properly. You must also add the following exec block into each question that is going to use the style.!-->")
  (forward-line)
  (insert "<exec>\nif device.smartphone:\n  ExpertDefinitionOE.styles.cs.resize = 1\n  ExpertDefinitionOE.styles.cs.width = 30\n</exec>")
  (forward-line)
  (insert "<!-- You can play around with the above values to fit your needs !-->"))

(defun keywordcode ()
  ;; inserts code needed for keyword coding
  (interactive)
  (insert "<exec when=\"virtualInit\">\nkeywordDataQ1 = File(\"keywordcode.dat\", \"key\")\n</exec>\n")
  (insert "<text\n  label=\"keywordDataAppendedQ1\">\n  <title>keywordcode Q1data</title>\n  <virtual>\n")
  (insert "otherData = keywordDataQ1.get(key)\n\nif otherData:\n  for r in data.rows:\n    rLabel = r.label.strip(\"r\")\n    r.val = otherData[\"header\" + rLabel]\n</virtual>\n")
  (setq counter 1)
  (while  (< counter 12) 
  (insert "  <row label=\"r" (format "%S" counter ) "\">Header " (format "%S" counter)  "</row>\n")
  (setq counter (+ counter 1)))
  (insert "</text>\n\n")
  (insert "<checkbox\n  label=\"vQ1Coded\">\n  <title>Where do you like to shop?</title>\n  <comment>Select all that apply</comment>\n  <virtual>\nfor r in keywordDataAppendedQ1.rows:\n    if r.val:\n        data.attr(\"r\" + r.val).val = 1\n  </virtual>\n  <net label=\"n999\" labels=\"r995,r996,r997,r998,r999\">MISCELLANEOUS</net>\n  <row label=\"r100\">99 Cent Store</row>\n  <row label=\"r101\">Amazon</row>\n  <row label=\"r102\">American Eagle</row>\n  <row label=\"r103\">Anne Taylor Loft/Loft</row>\n  <row label=\"r104\">Apple Store/Itunes</row>\n  <row label=\"r105\">Banana Republic</row>\n  <row label=\"r106\">Barnes &amp; Noble</row>\n  <row label=\"r107\">Best Buy</row>\n  <row label=\"r108\">Boutiques (Unspecified)</row>\n  <row label=\"r109\">Cabelas</row>\n  <row label=\"r110\">Chicos</row>\n  <row label=\"r235\">Stein Mart</row>\n  <row label=\"r236\">Local Store (Unspecified)</row>\n  <row label=\"r237\">Harley Davidson</row>\n  <row label=\"r238\">Saks Fifth Avenue</row>\n  <row label=\"r239\">Yarn Shop (Unspecified)</row>\n  <row label=\"r240\">Babies R Us</row>\n  <row label=\"r995\">Miscellaneous Comments</row>\n  <row label=\"r996\">Everything</row>\n  <row label=\"r997\">Nothing</row>\n  <row label=\"r998\">Don't know</row>\n  <row label=\"r999\">No detailed response</row>\n</checkbox>"))

(defun straighliner ()
  ;; inserts straighliner term
  (interactive)
  (insert "<term cond=\"any([c.all for c in Q1.cols])\">Q1 Term: Straightliner</term>"))

(defun na ()
  "Insert No Answer row"
  (interactive)
  (insert "<noanswer label=\"r99\">No Answer</noanswer>" )
  (forward-line))

(defun clean ()
  (goto-char 1)
  (interactive)
  (while (search-forward-regexp " id=\"[A-Za-z0-9][A-Za-z0-9][A-Za-z0-9][A-Za-z0-9][A-Za-z0-9]\"" nil t)
    (replace-match "" t nil))
  (goto-char 1)
  (while (search-forward-regexp ".*:\".*\" " nil t)
    (replace-match "" t nil))
  (goto-char 1)
  (while (search-forward-regexp "&lt;" nil t)
    (replace-match "<" t nil))
  (goto-char 1)
  (while (search-forward-regexp "&gt;" nil t)
      (replace-match ">" t nil)))

(defun states ()
  "Insert states dropdown question including D.C and the virtual Region question"
  (interactive)
  (insert "<select label=\"QS\">\n<title>In what state do you live?</title>\n<comment>Select an option from the drop down list.</comment>\n  <choice label=\"AL\">Alabama</choice>\n  <choice label=\"AK\">Alaska</choice>\n  <choice label=\"AZ\">Arizona</choice>\n  <choice label=\"AR\">Arkansas</choice>\n  <choice label=\"CA\">California</choice>\n  <choice label=\"CO\">Colorado</choice>\n  <choice label=\"CT\">Connecticut</choice>\n  <choice label=\"DE\">Delaware</choice>\n
  <choice label=\"DC\">District of Columbia</choice>\n  <choice label=\"FL\">Florida</choice>\n  <choice label=\"GA\">Georgia</choice>\n  <choice label=\"HI\">Hawaii</choice>\n  <choice label=\"ID\">Idaho</choice>\n  <choice label=\"IL\">Illinois</choice>\n  <choice label=\"IN\">Indiana</choice>\n  <choice label=\"IA\">Iowa</choice>\n  <choice label=\"KS\">Kansas</choice>\n  <choice label=\"KY\">Kentucky</choice>\n  <choice label=\"LA\">Louisiana</choice>\n  <choice label=\"ME\">Maine</choice>\n  <choice label=\"MD\">Maryland</choice>\n  <choice label=\"MA\">Massachusetts</choice>\n  <choice label=\"MI\">Michigan</choice>\n  <choice label=\"MN\">Minnesota</choice>\n  <choice label=\"MS\">Mississippi</choice>\n  <choice label=\"MO\">Missouri</choice>\n  <choice label=\"MT\">Montana</choice>\n  <choice label=\"NE\">Nebraska</choice>\n  <choice label=\"NV\">Nevada</choice>\n  <choice label=\"NH\">New Hampshire</choice>\n  <choice label=\"NJ\">New Jersey</choice>\n  <choice label=\"NM\">New Mexico</choice>\n  <choice label=\"NY\">New York</choice>\n  <choice label=\"NC\">North Carolina</choice>\n  <choice label=\"ND\">North Dakota</choice>\n  <choice label=\"OH\">Ohio</choice>\n  <choice label=\"OK\">Oklahoma</choice>\n  <choice label=\"OR\">Oregon</choice>\n  <choice label=\"PA\">Pennsylvania</choice>\n  <choice label=\"RI\">Rhode Island</choice>\n  <choice label=\"SC\">South Carolina</choice>\n  <choice label=\"SD\">South Dakota</choice>\n  <choice label=\"TN\">Tennessee</choice>\n  <choice label=\"TX\">Texas</choice>\n  <choice label=\"UT\">Utah</choice>\n  <choice label=\"VT\">Vermont</choice>\n  <choice label=\"VA\">Virginia</choice>\n  <choice label=\"WA\">Washington</choice>\n  <choice label=\"WV\">West Virginia</choice>\n  <choice label=\"WI\">Wisconsin</choice>\n  <choice label=\"WY\">Wyoming</choice>\n</select>\n<suspend/>\n\n<exec>\nNW = ['WA', 'OR', 'MT', 'ID', 'WY']\nSW = ['CA', 'NV', 'UT', 'AZ']\nMW = ['ND', 'SD', 'NE', 'MN', 'IA', 'MO', 'WI', 'IL', 'MI', 'IN', 'KY', 'OH', 'WV']\nCE = ['CO', 'NM', 'KS', 'OK', 'TX', 'AR', 'LA']\nNE = ['ME', 'VT', 'NH', 'MA', 'RI', 'CT', 'NJ', 'DE', 'MD', 'DC', 'PA', 'NY']\nSE = ['VA', 'NC', 'SC', 'FL', 'TN', 'MS', 'AL' ,'GA']\nOTHER = ['HI', 'AK']\n\nstate = QS.choices[QS.val].label\n\nif state in NW:\n  vRegion.val = vRegion.r1.index\nelif state in SW:\n  vRegion.val = vRegion.r2.index\nelif state in MW:\n  vRegion.val = vRegion.r3.index\nelif state in CE:\n  vRegion.val = vRegion.r4.index\nelif state in NE:\n  vRegion.val = vRegion.r5.index\nelif state in SE:\n  vRegion.val = vRegion.r6.index\nelif state in OTHER:\n  vRegion.val = vRegion.r7.index\n\n</exec>\n\n<radio label=\"vRegion\" where=\"execute\">\n<title>REGIONS</title>\n  <row label=\"r1\">NORTH WEST: WA, OR, MT, ID, WY</row>\n  <row label=\"r2\">SOUTH WEST: CA, NV, UT, AZ</row>\n  <row label=\"r3\">MID WEST: ND, SD, NE, MN, IA, MO, WI, IL, MI, IN, KY, OH, WV</row>\n  <row label=\"r4\">CENTRAL: CO, NM, KS, OK, TX, AR, LA</row>\n  <row label=\"r5\">NORTH EAST: ME, VT, NH, MA, RI, CT, NJ, DE, MD, DC, PA, NY</row>\n  <row label=\"r6\">SOUTH EAST: VA, NC, SC, FL, TN, MS, AL, GA</row>\n  <row label=\"r7\">OTHER: AK, HI</row>\n</radio>"))

(defun hideNextMulti ()
  "Insert JS Code to hide the next question until an answer has been provided for each row"
  (interactive)
  (insert "<style name=\"question.after\" wrap=\"ready\" label=\"showQues\" arg:firstQ=\"\" arg:secondQ=\"\">\n<![CDATA[\n  thisQ = $ ('#question_$(firstQ)');\n  nextQ = $ ('#question_$(secondQ)')\n  thisQRows = $ (\"#question_$(firstQ) .survey-q-grid tr\").length - 1;\nif (!thisQ.hasClass(\"hasError\")){\n  nextQ.hide();\n}\n\nthisQ.change(function () {\n  var selected = $ (\"input:radio:checked\");\n  if (selected.length == thisQRows){\n    nextQ.show(1000);\n  }\n  else{\n    nextQ.hide();\n  }\n})\n\n]]>\n</style>\n\n"))


(defun lro (start end)
  (interactive "r")
  " Make higlighted region into a looprow "
  (goto-char end)
  (setq endLineNum (what-line))
  (goto-char start)
  (setq curLineNum (what-line))
  (setq counter 1)

  (while (not (string-equal curLineNum endLineNum))
    (while (looking-at "[ ]")
      (delete-char 1))
    (if (looking-at "\n")
	(forward-line)
      (insert "  <looprow label=\"r" (format "%S" counter) "\"><loopvar name=\"\">")
      (end-of-line)
      (insert "</loopvar></looprow>")
      (forward-line)
      (setq curLineNum (what-line))
      (setq counter (+ counter 1))
      )))

(defun lva (start end)
  " Make highlighed region into loopvar "
  (goto-char start)
  (insert "<loopvar name=\"\">")
  (goto-char end)
  (insert "</loopvar>"))

(defun image (start end)
  " make highlighted region into image tags"
  (interactive "r")
  (goto-char end)
  (setq endLineNum (what-line))
  (goto-char start)
  (setq curLineNum (what-line))
  (while (not (string-equal curLineNum endLineNum))
    (while (looking-at "[ ]")
      (delete-char 1))
    (if (looking-at "\n")
	(forward-line)
      (insert "  <img src=\"")
      (end-of-line)
      (insert "\" alt=\"\"/>")
      (forward-line)
      (setq curLineNum (what-line))
      )))

(defun db ()
  "Insert survey db code"
  (interactive)
  (insert "<exec when=\"virtualInit\">\nfrom hermes.EmbeddedCode import Database\nmydb = Database(survey=\"other/project\", name=\"project_db\")\n</exec>\n"))


(defun markerRow ()
  (interactive)
  "Insert marker row autopopulate exec block - ie check marker name for row label name"
  (insert "<exec>\nprint p.markers\nfor eachRow in Q_Label.rows:\n  if hasMarker(\"/Quotas/*_quota_%s\" % eachRow.label):\n    Q_Label.val = eachRow.index\n</exec>\n"))


(defun markerCol ()
  (interactive)
  "Insert marker col autopopulate exec block - ie check marker name for col label name"
  (insert "<exec>\nprint p.markers\nfor eachCol in Q_Label.cols:\n  if hasMarker(\"/Quotas/*_quota_%s\" % eachCol.label):\n    Q_Label.val = eachCol.index\n</exec>\n"))


(defun itemize (n1 n2 p a)
 "Insert a list of numbers from a to b"
 (interactive "nStart Digit: \nnEnd Digit: \nsPrefix (r,ch,c,etc.): \nsPostfix: ")
 (if (< n1 n2)
   (while (or (< n1 n2) (= n1 n2))
     (insert  p (format "%S" n1) a "\n")
     (setq n1 (+ n1 1)))
   (if (> n1 n2)
     (while (or (> n1 n2) (= n1 n2))
        (insert (format "%S" p)(format "%S" n1) "\n")
        (setq n1 (- n1 1))))))


(defun nextSheet ()
  "Insert template code for moving respondents to another quota sheet when one is full"
  (interactive)
  (insert "<quota sheet=\"quota\" overquota=\"augment\"/>\n\n<if cond=\"0\">\n<label label=\"augment\"/>\n\n<quota sheet=\"Augment\"/>\n\n<exec>\n# remove OQ marker\n</exec>\n</if>"))

(defun favorite ()
  "Insert the favorite attribute"
  (interactive)
  (insert "favorite=\"1\""))


(defun rcss ()
  " Add respview.client.css style"
  (interactive)
  (insert "<style name=\"respview.client.css\">\n<![CDATA[\n<style>\n/*CSS HERE*/\n</style>\n]]>\n</style>"))

(defun rjs ()
  " Add respview.client.js style"
  (interactive)
  (insert "<style name=\"respview.client.js\" mode=\"after\" wrap=\"ready\">\n<![CDATA[\n<style>\n/*JS HERE*/\n</style>\n]]>\n</style>"))


(defun we ()
  " Insert the where = execute attribute" 
  (interactive)
  (insert " where=\"execute\""))

(defun e ()
  " Insert exec block "
  (interactive)
  (insert "<exec>\n\n</exec>"))

(defun v () 
  " Insert virtual block "
  (interactive)
  (insert "<virtual>\n\n</virtual>"))

(defun wn ()
  " Insert where = none "
  (interactive)
  (insert " where=\"none\""))

(defun stylet ()
  " Insert style tag "
  (interactive)
  (insert "<style>\n\n</style>"))

(defun cdata () 
  " Insert CDATA tags "
  (interactive)
  (insert "<![CDATA[\n\n\n]]>"))

(defun money ()
  " Monetary validate for text question "
  (interactive)
  (insert "<res label=\"decimal_error\">Please enter a valid decimal number in this format 999.99</res>\n\n<validatenif not gv.isSST():\n  if this.rX.val:\n    import re\n    deci = this.rX.val\n    if len(deci) ge 1:\n      if re.match(\"^-?[0-9]+\.[0-9]{2}$\", deci) == None:\n        error(res.decimal_error)\n</validate>"))

(defun sup(start end)
  " Superscript "
  (interactive "r")
  (goto-char end)
  (insert "</sup>")
  (goto-char start)
  (insert "<sup>"))

(defun wr ()
  " Where=report "
  (interactive)
  (insert " where=\"report\""))

(defun hidden()
  "Display question even if condition results in one row "
  (interactive)
  (insert "<row label=\"r10\" cond=\"1\" open=\"1\" openSize=\"25\" randomize=\"0\">Other (<i>please specify<i>)</row>\n  <row label=\"hidden\" ss:rowClassNames=\"hidden\" where=\"survey,notdp\">Please specify:</row></radio>\n<style mode=\"after\" name=\"respview.client.js\" wrap=\"ready\"><![CDATA[\n$ (\"td.hidden\").remove();"))



(defun speeder (seconds)
  "Insert speeder term based on completions of less than seconds"
  (interactive "nSeconds: ")
  (insert "<term cond=\"timeSpent() lt "(format "%S" seconds)" and not gv.isSST()\">Speeder: Less than "(format "%S" seconds)".</term>\n"))
  

(defun val ()
  "Insert validate tags"
  (interactive)
  (insert "<validate>\n\n</validate>"))


(defun prettySelect ()
  """ Insert prettySelect code for dropdown """
  (interactive)
  (insert "<style mode=\"after\" name=\"respview.client.css\"><![CDATA[\n<style>\n.prettySelect .grid-desktop-mode.grid-table-mode .nonempty{\n  border: none !important;\n  border: 2px solid blue;\n}\n.prettySelect .input  {\n   -webkit-border-radius: 5px;\n   -moz-border-radius: 5px;\n   border-radius: 5px;\n}\n\n\n.prettySelect .input{\n   height: 29px;\n   overflow: hidden;\n   width: 240px;\n}\n\n.prettySelect select {\n   background: transparent;\n   border: 1px solid #d9d9d9;\n   height: 29px;\n   padding: 5px; /* If you add too much padding here, the options won't show in IE */\n   width: 268px;\n}\n</style>\n]]></style>\n"))

(defun zipcodes ()
  """ Zipcode info from file """
  (interactive)
  (insert "<exec when=\"init\">\n  from hermes.virtual import KeyedFile\n  zipcodes = KeyedFile(\"int/zipcode/zip.txt\", \"ZipCode\")\n  </exec>\n"))

  
(defun headers()
  """ CSS FOR HAVING COLUMN HEADERS BOTTOM ALIGNED """
  (interactive)
  (insert ".grid-desktop-mode .col-legend{\nvertical-align: bottom;\n}\n"))

