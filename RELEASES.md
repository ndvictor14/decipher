Updates in v2.0 (July 22,2015)
===================
##### NEW
dynamic.el: New functions for quickly inserting our DQ questions - functions listed below

Function Name | Action
speeder | Speeder term based on seconds (prompts for seconds)|
val | Insert validate Tags
rjs | Insert respview.js block
rcss | Insert respview.css block
radbuttonselect| Turn region into radio atm1d.6 - button select
chebuttonselect| Turn region into checkbox atm1d.6 - button select
maxdiff | Insert max diff template
dcm | Insert Dynamic Choice Model Template
headers | Insert css to bottom align headers (only adds css, not block so use rcss first if needed)


#####  Updates:
* decipher.el: Updated compat level
* bulk: updated privacy link
* itemize: now supports post text entries

Updates in v1.8 (December  5,2014)
====================
#####  NEW
######key bindings! : See keybindings.md for more information
Function Name | Action
---------|-----
**v** | Add virtual tags|
**e** | Add exec tags|
**wn** | Add where="none"|
**stylet** | Insert style tags |
**cdata** | Insert CDATA block|


Updates in v1.7 (October 24,2014)
=========
#####  *NEW*
Function Name | Action
----------|------------
**markerRow** | exec block that checks for marker labels matching row labels and autofills the question|
**markerCol** | same as markerRow but for col|
**itemize** | creates a list of variables witha a prepended, defined string, and numbers with in a range and in defined increments|
**nextSheet** | move respondents to a different quota sheet once the first is filled|
**db** | insert database usage code|
**favorite** | insert favorite attribute|
**.screenrc has been added to escape ****ctrl**-**a** with **ctrl**-**\**** , this enables you to use ctrl-a in a natural way**
 
#####  Updates:
  * Fixed mr and mc so that they no longer rely on regex matching and just go based off of the region specified

Updates in v1.6 (October 15, 2014)
========
#####  NEW
Function Name | Action
-----------|-----------
showExec | adds a respview.client.js block to automatically show the green exec blocks|
loop | insert a loop template|
lro | make region into a looprow|
ca -> pi | The "ca" function was changed to "pi" for readability. "ca" is now used to make text into cases.|
image | make region lines into img tags|
  ** mouse scrolling on by default **

##### Updates
  * Fixed pi where first case was being moved to the first line

Updates in v1.5 (July 7,2014)
==========
#####   NEW
Function Name | Action
------------|----------
states | Adds dropdown of states including D.C and maps the regions|
mf | Makes selection into a float question|

#####   Updates
   * Fixed ma where the question was not being properly created

Updates in v1.4 (May 19, 2014)
==========
#####   NEW
Function Name | Action
-------------|---------
mrc | make report comment |
mop | add mobile optimization code for those large grid questions|
keywordcode | add code for keyword coding|
straightliner | adds a term for straighlining respondents |
clean | Removes id's added on by builder and the xlmns nonsense..|
hideNextMulti | Inserts JS code to hide the following question until each row in radio grid has an answer|

#####   Updates
   * Fixed mh - html call to add where="survey"
   * updated mn to include optional="0" and size="3"
   * Fixed bugs with alt, noanswer, and oe
   * mv - updated instruction text to "Please select one for each row." (was "Please select one.")
   * Updated compat to 121
   * Fixed ms bug


Updates in v1.3 (April 10, 2014):
===========
#####    NEW
Function Name | Action
-------------|---------
bulk | create the bulk-email file just like in NoteTab, no more switching between programs! :)|
privacy | override the privacy policy and help links|

Updates in v1.2.2 (April 9, 2014):
=======
    * Added <u>, <b>, & <i> functions (escaped and non-escaped)
    * Fixed bug with mh and mn where inserts where not working properly

Updates in v1.2:
=======
#####NEW
Function Name | Action
-------------|---------
pt | inserts an exec block (created by Shaen Toner) that tracks the time respondents spend on each individual page|
ac | inserts the "allowCountries" tag (to be placed inside survey tag)|
fc | inserts the "forbiddenCountries" tag (to be placed inside the survey tag)|
canzip | validates Canadian Postal Codes|
asoqc | Prevents autosave returning respondents from completing the survey if the quotas have been met. (created by Sandra)|
tt | turns the selection into a tooltip radio question (SMART interface, no need to create rows ahead of time)|


Upadates in v1.1:
=======
#####    NEW
Function Name | Action
-------------|---------
wm | inserts the watermark execute block, make sure you have the images in a protected directory !|
##### Updates
* Fixed bug in quota tag where tag wasn't closed


The NEW Stuff in v1.0:
Function Name | Action
-------------|---------
shc | adds 'shuffle="cols"'|
hideNext | inserts code needed to hide the next part of the question until the previous is answered (insert it inside the question itself people)|
il | inserts image links (<a href ...><img src.....>)|
gt | insert the "goto" block|
vzip | Inserts a validate that checks for either 5-digit zip codes or extended zip codes|
qt | inserts standard quota tag|
for full documentation on functions please see the FUNCTIONS.txt file|


Upcoming updates: 
Along with the bug fixes this is what I plan: 

  1. Smart Questions: Won't need to pre make rows/cols
  2. Dynamic Pre-loaded Questions: Will insert appropriate "uses=" attribute

####**If you would like to see anything else, again please email: vhernandez@decipherinc.com**