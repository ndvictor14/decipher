;; .emacs
(require 'flymake)
(global-set-key [f3] 'flymake-display-err-menu-for-current-line)
(global-set-key [f4] 'flymake-goto-next-error)

;; Mouse Functionality
(require 'mouse)
(xterm-mouse-mode t)
(defun track-mouse (e)) 
(setq mouse-sel-mode t)
(mouse-wheel-mode t)

;; === line by line scrolling 
(setq scroll-step 1)

(defun scroll-up-mouse ()
 "Scroll up via mousewheel"
 (interactive)
 (scroll-up 2))
(defun scroll-down-mouse ()
 "Scroll down via mousewheel"
 (interactive)
 (scroll-down 2))

(global-set-key (kbd "<mouse-4>") 'scroll-down-mouse)
(global-set-key (kbd "<mouse-5>") 'scroll-up-mouse)

  ;; scroll one line at a time (less "jumpy" than defaults)
    
    (setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
    
    (setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
    
    (setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
    
    (setq scroll-step 1) ;; keyboard scroll one line at a time

;; split on middle button
(global-set-key (kbd "<mouse-3>") 'split-window-right)


;;; Auto enable flymake when possible
(add-hook 'find-file-hook 'flymake-find-file-hook)

;;; uncomment this line to disable loading of "default.el" at startup
;; (setq inhibit-default-init t)

;; enable visual feedback on selections
;(setq transient-mark-mode t)

;; default to better frame titles
(setq frame-title-format
      (concat  "%b - emacs@" (system-name)))

;; default to unified diffs
(setq diff-switches "-u")

;; always end a file with a newline
;(setq require-final-newline 'query)

;;; uncomment for CJK utf-8 support for non-Asian users
;; (require 'un-define)


;; uncomment to set highlight current line mode
;; (global-hl-line-mode 1)


;; === Auto load abbreviations table === 

;; emacs , by default, looks for "~/.abbrev_defs" ie. DON'T CHANGE THE NAME
(setq-default abbrev-mode t)
(read-abbrev-file "~/.abbrev_defs")
(setq save-abbrevs t)

;; Load my Function File
(load "~/.emacs.d/decipher.el")
(load "~/.emacs.d/d2.el")
(load "~/.emacs.d/webDev.el")
(load "~/.emacs.d/flymake.el")

;; Note: to see list of current abbreviations enter LIST-ABBREVS

;; === Set default indent to 2 instead of 4 ===
(setq standard-indent 2)



;; No auto-save
(setq make-backup-files nil)
(setq auto-save-default nil)

;; Ruler 
(require 'linum)
(line-number-mode 1)
(column-number-mode 1);; line numbers on left
(global-linum-mode 1)

;; set fill comlumn and enable auto fill
(setq auto-fill-mode 1)


;; open compressed and tarballs like a directory
(auto-compression-mode 1)

;; enable remote diractory access (me@remotehost:path)
(setq tramp-default-method "ssh")

;; ==== Set COLORS :D ===

;; Set cursor and mouse-pointer colours
(set-cursor-color "red")
(set-mouse-color "goldenrod")

;; Set region background colour
(set-face-background 'region "blue")

;; Set emacs background colour
(set-background-color "black")

;; template stuff
(setq user-mail-address "vhernandez@decipherinc.com")
(setq user-website "http://v2.decipherinc.com") 
(put 'upcase-region 'disabled nil)


