;; This file includes decipher dynamic question templates complete with all possible attributes
(load "~/.emacs.d/d2.el");
(load "~/.emacs.d/decipher.el");
(defun test(type,dq)
  (interactive "r")
  (mr)
)

(defun radbuttonselect (start end)
  ;; Button Select - atm1d.6 - radio
  (interactive "r")
  (goto-char end)
  (forward-line)
  (insert "</radio>\n<suspend/>\n")
  (setq endLineNum (what-line))
  (goto-char start)
  (setq curLineNum (what-line))
  (setq counter 1)
  (while (looking-at "[ ]")
    (delete-char 1))
  (if (looking-at "[0-9]+")
      (insert "<radio label=\"Q")
    (if (looking-at "[A-Za-z]+[0-9]+")
	(insert "<radio label=\"")
      (if (looking-at "[A-Za-z]+")
	  (insert "<radio label=\"")
	(message "Check your syntax: Label (space) title (newline) elements"))))
  
  (search-forward-regexp "\s") 
  (backward-char 1)
  (insert "\" uses=\"atm1d.6\" atm1d:oneLine=\"0\" atm1d:one_col=\"0\" atm1d:showInputs=\"0\">\n  <title>")
  (delete-char 1)
  (end-of-line)
  (insert "</title>\n  <comment>Select one.</comment>")
  (while (looking-at "\n")
      (delete-region (point) (progn (forward-line 1) (point))))
  (insert "\n"))


(defun chebuttonselect (start end)
  ;; Button Select - atm1d.6 - checkbox
  (interactive "r")
  (goto-char end)
  (forward-line)
  (insert "</checkbox>\n<suspend/>\n")
  (setq endLineNum (what-line))
  (goto-char start)
  (setq curLineNum (what-line))
  (setq counter 1)
  (while (looking-at "[ ]")
    (delete-char 1))
  (if (looking-at "[0-9]+")
      (insert "<checkbox label=\"Q")
    (if (looking-at "[A-Za-z]+[0-9]+")
	(insert "<checkbox label=\"")
      (if (looking-at "[A-Za-z]+")
	  (insert "<checkbox label=\"")
	(message "Check your syntax: Label (space) title (newline) elements"))))
  
  (search-forward-regexp "\s") 
  (backward-char 1)
  (insert "\" uses=\"atm1d.6\" atm1d:oneLine=\"0\" atm1d:one_col=\"0\" atm1d:showInputs=\"0\">\n  <title>")
  (delete-char 1)
  (end-of-line)
  (insert "</title>\n  <comment>Select all that apply.</comment>")
  (while (looking-at "\n")
      (delete-region (point) (progn (forward-line 1) (point))))
  (insert "\n"))




(defun ranksort (start end)
  "Insert ranksort DQ"
  (interactive "r")
  (goto-char end)
  (forward-line)
  (insert "</select>\n<suspend/>\n")
  (setq endLineNum (what-line))
  (goto-char start)
  (setq curLineNum (what-line))
  (setq counter 1)
  (while (looking-at "[ ]")
    (delete-char 1))
  (if (looking-at "[0-9]+")
      (insert "<selct label=\"Q")
    (if (looking-at "[A-Za-z]+[0-9]+")
	(insert "<select label=\"")
      (if (looking-at "[A-Za-z]+")
	  (insert "<select label=\"")
	(message "Check your syntax: Label (space) title (newline) elements"))))
  
  (search-forward-regexp "\s") 
  (backward-char 1)
  (insert "\" uses=\"ranksort.2\" ranksort:btnOpenEdit=\"edit\" ranksort:showBucketNumber=\"1\" ranksort:ranksortContainerCSS=\"\" ranksort:answersContainerCSS=\"\" ranksort:cardsContainerCSS=\"\" ranksort:cardCSS=\"\" ranksort:cardHoverCSS=\"\" ranksort:cardDroppedCSS=\"\" ranksort:cardStateDisabledCSS=\"\" >\n  <title>")
  (delete-char 1)
  (end-of-line)
  (insert "</title>\n  <comment>Select all that apply.</comment>")
  (while (looking-at "\n")
      (delete-region (point) (progn (forward-line 1) (point))))
  (insert "\n"))
  



(defun maxdiff ()
  " MAX DIFF TEMPLATE CODE "
  (interactive)
  (insert "<!-- res labels -->\n\n<res label=\"Q#_mditem#\">ATTRIBUTE 1</res>\n\n  <exec when=\"init\">\nimport csv\nclass MAXDIFF:\n    def __init__(self, fname, fileDelimiter=\"\t\"):\n        csvObj = csv.reader(open(\"%s/%s\" % (gv.survey.path, fname)), delimiter=fileDelimiter)\n        self.d = dict( (\"v%s_t%s\" % (row[0], row[1]), row[2:]) for row in csvObj )\n\n    def setItemsA(self, vt, question, parentLabel):\n        items = self.d[vt]\n\n        print \"*****STAFF ONLY*****\"\n        print \"Version_Task: %s\" % vt\n        for r in question.rows:\n            r.text = res[ \"%s_mditem%s\" % (parentLabel, items[r.index]) ]\n            print \"Item %s: %s\" % (r.index+1, items[r.index])\n\n    def setItemsI(self, vt, question):\n        item_index = dict( (r.o.label.strip(\"item\"), r.index) for r in question.rows )\n\n        items = self.d[vt]\n\n        for r in question.rows:\n            if r.o.label.strip(\"item\") not in items:\n                r.disabled = True\n\n        question.rows.order = [ item_index[i] for i in items ]\n\n        print \"*****STAFF ONLY*****\"\n        print \"Version_Task: %s\" % vt\n        for i in range(len(items)):\n            print \"Item %s: %s\" % (i+1,items[i])\n</exec>\n")

  (insert "  <exec when=\"init\">\nQ#_md = MAXDIFF(\"design.txt\")\n</exec>")
  (insert "<quota overquota=\"noqual\" sheet=\"Q#_MaxDiff\"/>")
  (insert "<number label=\"Q#_Version\" size=\"3\" verify=\"range(1,16)\" where=\"execute,survey,report\">\n    <title>Q# - MaxDiff Version</title>\n    <exec>\nprint p.markers\nfor x in p.markers:\n  if \"/Q#_MaxDiff/ver_\" in x:\n    Q#_Version.val = int(x.split(\"_\")[-1])\n    break\n  </exec>\n  </number>\n  <suspend/>")
  (insert "\n\n\n  <exec>\np.startTime = time.time()\n</exec>\n
  <loop label=\"Q1_md_loop\" vars=\"task\">\n    <title>Q1 - MaxDiff Loop</title>\n    <block label=\"Q1_md_block\" randomize=\"1\">\n      <radio label=\"Q1_[loopvar: task]_[loopvar: label]\" adim=\"cols\" grouping=\"cols\" ss:questionClassNames=\"Q1_maxdiff\" unique=\"1\">\n        <title><span style=\"color: navy; text-align: center;\">For each set, please indicate which is the most important <br />priority for America and which issue is the least important priority <br />for America.</span></title>\n        <comment><span style=\"color:navy; width:100%; text-align: center !important;\">Please move your cursor over the name of each issue to read a definition of the issue.</span></comment>\n        <exec>\nQ1_md.setItemsA( \"v%s_t%d\" % (Q1_Version.val, [loopvar: task]), Q1_[loopvar: task]_[loopvar: label], \"Q1\")\np.MDcount = str(Q1_md_loop_expanded.order.index([loopvar: task]-1)+1)\n      </exec>\n        <col label=\"best\">Most <br />Important</col>\n        <col label=\"worst\">Least <br />Important</col>\n        <row label=\"item1\">item 1</row>\n        <row label=\"item2\">item 2</row>\n        <row label=\"item3\">item 3</row>\n        <row label=\"item4\">item 4</row>\n        <row label=\"item5\">item 5</row>\n        <style mode=\"before\" name=\"question.element\">\n<style type=\"text/css\">\nh2, h3{\n  margin-left: 190px;\n  max-width: 500px;\n  text-align: center;\n}\n.survey-q-grid.setWidth{\n  margin-left: 200px;\n  width: 480px;\n}\n</style>\n</style>\n        <style mode=\"before\" name=\"question.header\">\n<style type=\"text/css\">\n\n.Q1_maxdiff .survey-q-grid .survey-q-grid-rowlegend {\n    text-align: center;\n}\n\n.empty{\n  border: 1px solid black !important;\n   background-color: #00316E !important;\n   color: white;\n}\n\n\n#Q1_maxdiff_worst, #Q1_maxdiff_best{\n\n}\n</style>\n</style>\n        <style name=\"question.row\">\n<style type=\"text/css\">\n.survey-q-grid .even td{\n  background-color: #FFFFFF !important;\n}\n\n.survey-q-grid .odd td{\n  background-color: #D2E3F7 !important;\n}\n\n</style>\n\n\@if this.styles.ss.rowHeight\n    <tr class=\"$(style) colCount-$(colCount)\" style=\"height:${this.styles.ss.rowHeight};\">\n\@else\n    <tr class=\"$(style) colCount-$(colCount)\">\n\@endif\n    ${elements.split(\"</td>\")[0]}</td>\n    $(left)\n    ${elements.split(\"</td>\")[1]}</td>\n</tr>\n</style>\n        <style name=\"question.top-legend\">\n<style type=\"text/css\">\nth{\n  background-color: #00316E !important;\n   border: 1px solid black !important;\n   background-color: #00316E !important;\n   color: white;\n   font-weight: bold !important;\n   width: 10px !important;\n}\n</style>\n\@if this.styles.ss.colLegendHeight\n    <tr class=\"legend top-legend${\" GtTenColumns\" if ec.colCount > 10 else \"\"} colCount-$(colCount)\" style=\"height:${this.styles.ss.colLegendHeight};\">\n\@else\n    <tr class=\"legend top-legend${\" GtTenColumns\" if ec.colCount > 10 else \"\"} colCount-$(colCount)\">\n\@endif\n    ${legends.split(\"</th>\")[0]}</th>\n    $(left)\n    ${legends.split(\"</th>\")[1]}</th>\n</tr>\n<tbody>\n</style>\n      </radio>\n    </block>\n    <looprow label=\"1\">\n      <loopvar name=\"task\">1</loopvar>\n    </looprow>\n    <looprow label=\"2\">\n      <loopvar name=\"task\">2</loopvar>\n    </looprow>\n    <looprow label=\"3\">\n      <loopvar name=\"task\">3</loopvar>\n    </looprow>\n    <looprow label=\"4\">\n      <loopvar name=\"task\">4</loopvar>\n    </looprow>\n    <looprow label=\"5\">\n      <loopvar name=\"task\">5</loopvar>\n    </looprow>\n    <looprow label=\"6\">\n      <loopvar name=\"task\">6</loopvar>\n    </looprow>\n    <looprow label=\"7\">\n      <loopvar name=\"task\">7</loopvar>\n    </looprow>\n    <looprow label=\"8\">\n      <loopvar name=\"task\">8</loopvar>\n    </looprow>\n    <looprow label=\"9\">\n      <loopvar name=\"task\">9</loopvar>\n    </looprow>\n    <looprow label=\"10\">\n      <loopvar name=\"task\">10</loopvar>\n    </looprow>\n    <looprow label=\"11\">\n      <loopvar name=\"task\">11</loopvar>\n    </looprow>\n    <looprow label=\"12\">\n      <loopvar name=\"task\">12</loopvar>\n    </looprow>\n    <looprow label=\"13\">\n      <loopvar name=\"task\">13</loopvar>\n    </looprow>\n    <looprow label=\"14\">\n      <loopvar name=\"task\">14</loopvar>\n    </looprow>\n    <looprow label=\"15\">\n      <loopvar name=\"task\">15</loopvar>\n    </looprow>\n  </loop>\n  <float label=\"Q1_Timer\" size=\"15\" where=\"execute,survey,report\">\n    <title>Q1 - MaxDiff Timer (Minutes)</title>\n    <exec>\nQ1_Timer.val = (time.time() - p.startTime) / 60.0\n  </exec>\n  </float>\n  <exec>\ndel p.startTime\ndel p.MDcount\n</exec>\n\n  )\n"))


(defun dcm ()
  "DCM TEMPLATE"
  (interactive)
  (insert "<exec when=\"init\">\ndef setupDCMFile(fname, fileDelimiter=\"\t\"):\n    f = open(\"%s/%s\" % (gv.survey.path, fname))\n    dcmObj = [ line.strip(\"\\r\").split(fileDelimiter) for line in f.readlines() ]\n\n    d = {}\n    dcm_concepts = []\n\n    for i,row in enumerate(dcmObj):\n        if i:\n            d[\"v%s_t%s_c%s\" % (row[0],row[1],row[2])] = row[3:]\n            if row[2] not in dcm_concepts:\n                dcm_concepts.append(row[2])\n\n    concepts = [ int(x) for x in dcm_concepts ]\n    concepts.sort()\n    d[\"concepts\"] = dcm_concepts\n\n\n    return d\n\n#set persistent items, format: p.concept#_att#\ndef setupDCMItems(d, vt, prefix=\"1\"):\n    print \"***** STAFF ONLY *****\"\n    print \"***** DCM Matrix *****\"\n    print \"Version_Task: %s\" % vt\n\n    for concept in d.get(\"concepts\"):\n        attributes = d[ \"%s_c%s\" % (vt,concept) ]\n        print \"Concept %s: %s\" % (concept,attributes)\n\n        for i,attr in enumerate(attributes):\n            p[ \"concept%s_att%s\" % (concept,i+1) ] = res[ \"%s_att%s_level%s\" % (prefix,i+1,attr) ]\n            p[ \"dcmLegend_att%s\" % (i+1) ] = res[ \"%s_legend%s\" % (prefix,i+1) ]\n</exec>\n\n<exec when=\"init\">\nQ1_dcm = setupDCMFile(\"design.txt\")\n</exec>\n\n<quota overquota=\"noqual\" sheet=\"Q1_DCM\"/>\n\n<number label=\"Q1_Version\" size=\"3\" optional=\"1\" verify=\"range(1,5)\" where=\"execute\">\n  <title>Q1 - DCM Version</title>\n  <exec>\nprint p.markers\nfor x in p.markers:\n  if \"/Q1_DCM/ver_\" in x:\n    Q1_Version.val = int(x.split(\"_\")[-1])\n    break\n  </exec>\n</number>\n<suspend/>\n\n<res label=\"Q1_legend1\">Legend 1</res>\n<res label=\"Q1_legend2\">Legend 2</res>\n<res label=\"Q1_legend3\">Legend 3</res>\n<res label=\"Q1_legend4\">Legend 4</res>\n<res label=\"Q1_legend5\">Legend 5</res>\n\n<res label=\"Q1_att1_level1\">att1-1</res>\n<res label=\"Q1_att1_level2\">att1-2</res>\n<res label=\"Q1_att1_level3\">att1-3</res>\n<res label=\"Q1_att1_level4\">att1-4</res>\n<res label=\"Q1_att2_level1\">att2-1</res>\n<res label=\"Q1_att2_level2\">att2-2</res>\n<res label=\"Q1_att2_level3\">att2-3</res>\n\n<res label=\"Q1_att3_level1\">att3-1</res>\n<res label=\"Q1_att3_level2\">att3-2</res>\n<res label=\"Q1_att3_level3\">att3-3</res>\n\n<res label=\"Q1_att4_level1\">att4-1</res>\n<res label=\"Q1_att4_level2\">att4-2</res>\n<res label=\"Q1_att4_level3\">att4-3</res>\n<res label=\"Q1_att5_level1\">att5-1</res>\n<res label=\"Q1_att5_level2\">att5-2</res>\n<res label=\"Q1_att5_level3\">att5-3</res>\n\n<exec>p.startTime = time.time()</exec>\n\n<loop label=\"Q1_dcm_loop\" vars=\"task\" randomizeChildren=\"0\">\n  <title>Q1 - DCM Loop</title>\n  <block label=\"Q1_dcm_block\" randomize=\"1\">\n    <radio label=\"Q1_[loopvar: task]\" optional=\"0\" style=\"dcm\"\n      ss:questionClassNames=\"Q1_dcm\"\n      dcm:attributes=\"5\"\n      dcm:legend=\"1\"\n      dcm:top=\"Concepts\"\n      dcm:row=\"Select one option\"\n      <title>DCM Title [DCMcount]</title>\n      <alt>DCM Task: [loopvar: task]</alt>\n      <comment>Select one</comment>\n      <exec>\nsetupDCMItems( Q1_dcm, \"v%s_t%s\" % (Q1_Version.val,\"[loopvar: task]\"),\"Q1\" )\np.DCMcount = \"%d\" % (Q1_dcm_loop_expanded.order.index([loopvar: task]-1) + 1)\n      </exec>\n      <col label=\"c1\">Concept 1</col>\n      <col label=\"c2\">Concept 2</col>\n      <col label=\"c3\">Concept 3</col>\n      <col label=\"c4\">Concept 4</col>\n      <style name=\"question.header\" mode=\"before\">\n        <![CDATA[\n          <style type=\"text/css\">\n/* add this only if you have scrollbars in IE7,8\ndiv.Q1_dcm {\n    overflow: hidden;\n}\n*/\n.Q1_dcm tr.legend th.legend {\n    font-weight: bold;\n    width: auto;\n}\n.Q1_dcm th, .Q1_dcm td {\n    padding: 15px;\n}\n.Q1_dcm tr.dcm_even {\n    background-color: #FFFFFF;\n}\n.Q1_dcm tr.dcm_odd {\n    background-color: #EFEFEF;\n}\n.Q1_dcm td.dcm_legend {\n    font-weight: bold;\n    text-align: left;\n    width: 120px;\n}\n.Q1_dcm tr.dcm_even td.dcm_item, .Q1_dcm tr.dcm_odd td.dcm_item {\n    text-align: center;\n    width: 120px;\n}\n          </style>\n        ]]>\n      </style>\n\n    </radio>\n    <suspend/>\n  </block>\n\n  <looprow label=\"1\"> <loopvar name=\"task\">1</loopvar> </looprow>\n  <looprow label=\"2\"> <loopvar name=\"task\">2</loopvar> </looprow>\n  <looprow label=\"3\"> <loopvar name=\"task\">3</loopvar> </looprow>\n  <looprow label=\"4\"> <loopvar name=\"task\">4</loopvar> </looprow>\n  <looprow label=\"5\"> <loopvar name=\"task\">5</loopvar> </looprow>\n</loop>\n\n<float label=\"Q1_Timer\" size=\"15\" where=\"execute\">\n  <title>Q1 - DCM Timer (Minutes)</title>\n  <exec>Q1_Timer.val = (time.time() - p.startTime) / 60.0</exec>\n</float>\n\n<exec>\ndel p.startTime\ndel p.DCMcount\n</exec>\n"))
