;;; This file contains commonly used redirects

(defun SSI ()
  """ SSI REDIRECTS """
  (interactive)
  (insert "<samplesource list=\"\">\n  <title>SSI</title>\n  <completed>It seems you have already entered this survey.</completed>\n  <invalid>You are missing information in the URL. Please verify the URL with the original invite.</invalid>\n  <var name=\"psid\" unique=\"1\"/>\n  <exit cond=\"qualified\" url=\"http://dkr1.ssisurveys.com/projects/end?rst=1&amp;psid=${psid}&amp;basic=YYYY\"/>\n  <exit cond=\"terminated\" url=\"http://dkr1.ssisurveys.com/projects/end?rst=2&amp;psid=${psid}\"/>\n  <exit cond=\"overquota\" url=\"http://dkr1.ssisurveys.com/projects/end?rst=3&amp;psid=${psid}\"/>\n</samplesource>\n\n" ))
