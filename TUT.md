Emacs Tutorial v1.0
=========
##emacs is a file editor just like vim. The reason I use emacs is becuase it is more intuitive for me. This file explains some of the basic features emacs has to offer. Let's get started.

* Where vim uses "**:**" to start commands, emacs uses **CTRL**-**x**

* One thing to note is that unlike vim, emacs has a built in autosave function. This results in files "filename~" or "#filename#" being created. I have turned this feature off in my .emacs file because it is unnecessary in our work. I recommend you do the same. 

* Another key distinction that will cause frustration if you don't make the distinction is that unlike vim, emacs is not inclusive of what ever is at the cursor. What this means is that if you want to highlight something, start either a position before, or a position after depending on the direction you're going! I warned you! 

* To open a file:
   $> **emacs filename**

  **Note: entering multiple filename will create different "windows" (from here on referred to by their name: buffer) for each file**

* To manually open a different buffer:
   While inside a file: 
   **CTRL**-**2** : opens a horizontal buffer below the current one
   **CTRL**-**3** : opens a vertical buffer to the right
 

  Now that you have different buffers open you'll probably want to be able to switch between them right?
  **CTRL**-**o** : Think of this as control other because it jumps to the "other" buffer!
  What's this, you opened up an additional buffer on accident? **CTRL**-**0** (zero) and BYE-BYE buffer!

* Opening a different file in a buffer
  Say you want to look at a particular part of one file and compare it to your file. To open the other file in a different buffer, navigate to the buffer then **CTRL**-**f**. This will prompt you with your current path, from there you can navigate to the file you want to open. 
  Is your file on a different *server*? Specify the path /you@host.com:/path/to/file.
  Need to autocomplete / search on the remote server? Tab will prompt you for your password and you can go from there. 

* Saving/Closing Files:
  **CTRL**-**x** **CTRL**-**s** : Saves file 
  **CTRL**-**x** **CTRL**-**c** : Closes file (If you haven't saved it, it will prompt you whether you would like to save it)

* Highlighting:
The highlighted part of the text is called the region. You can set the "mark" to start highlighting with **CTRL**-**space** (you will see a message at the bottom)
From there you can move around with the cursor. Now, highligting is a big part of efficiency so you should know some of the shortcuts. I will provide a few major ones here: 
     1. In order to highlight how lines with a shortcut key a key-binding has to be set up. I have not done that so here's a way around it. With the mark set (**CTRL**-**space**) you can (**CTRL**-**e**) to go to the end of line and your line is now highligted!
     2. Forgot to highlight something at the other end of the mark? You don't want to have to highlight the whole thing all over! (**CTRL**-**x**) (**CTRL**-**x**) will move the cursor to the mark (the initial location) and you will be free to continue your highlighting!
     3. My favorite. Killing a Rectangular Region. Emacs users may have found vim to be magical when they discovered the "delete rectangular region" functionality. Emacs has this functionality too. Highlight the rectangular region you want, dont' worry, emacs highlights the lines completely, we're smart, look at the rectangle you're making. Then (**CTRL**-**x**) (**r**) (**k**) and BOOM! Rectangle GONE! Simplified Explanation: Execute, rectangle, kill!
     4. To highlight everything, you can set the marker at either end of the document then to go to the other end: if starting from the top (**Esc**-**>**) will get you to the bottom & if starting from the bottom (**Esc**-**<**) will get you to the top! 

* Copy/Pasting/Undo/Redo:
Nice and easy section:
     1. To remove an entire line or anything in front of the cursor up to the end of line (**CTRL**-**k**) : KILL!
     2. You know that line or bit of line we just killed? I want it back.. maybe multiple time (**CTRL**-**y**) will YANK it from Death himself, as many times as needed. Try it out!
     3. What? You want to undo the mess you made? (**CTRL**-**_**) will do just that. 
     4. Back and forth, to redo (**CTRL**-**x** **u**) - now this can be used as undo as well but (**CTRL**-**_**) does one entry in the current buffer's undo records. 
     
