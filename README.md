Decipher Emacs Functions v1.0
=================

FOR UPDATE NOTES SEE THE RELEASES.txt file.
---------
##INSTALLATION:
  1. Download the "decipher.el" file (or copy to clipboard)
  2. In your home directory, if you haven't already, create a .emacs.d directory (mkdir .emacs.d)
  3. Go into the .emacs.d directory (cd .emacs.d)
  4(Windows) Add your file in this directory using FileZilla or whatever tool you use. If you copied it to your clipboard, (emacs decipher.el) then, (SHIFT+RIGHT_CLICK) & VOILA! Proud owner of the decipher.el file. 
  4(UNIX). For those of you on MacOS/UNIX I'm sure you're familiar with scp so go ahead and upload the downloaded file that way!
  5. Next, you'll want to make sure emacs is reading this file so you have access to the functions. To do this let's first see if you have a .emacs file:
  - In the current directory, (emacs ~/.emacs)
  - If the file is blank, you didn't have one. If you want mine it's one of the files in the repository and you can follow steps 1-4 for it.
  - If it's not blank and you have your own configurations simply add the line "(load "~/.emacs.d/decipher.el")" [without the outer quotes]
  6. Repeat these steps for the "d2.el and dynamic.el" (optional flymake.el) files. The d2 file has the functions to add single attributes to the questions, such as "open='1'", "shuffle='rows'", etc. Make sure you load the file in your .emacs file! The flymake.el file is optional. It is a nifty tool that has syntax error highlighting. You can find out more about it here: cvs.savannah.gnu.org/viewvc/emacs/emacs/lisp/progmodes/flymake.el
  7. Leave any feedback at vhernandez@decipherinc.com - ENJOY!


####NOTES:

The "decipher.el" file contains the functions used in vim for survey snippets. 
The command shortcuts are the same as those in vim to allow for easier transitions from editor to editor.

##### ** Executing functions on windows: (Alt-x functionName) **






Happy Programming!
